const express = require("express");
const cors = require('cors')
require('dotenv').config();

// const rawMoviesData = require('./Data/IMDBMovies.json')
// const rawReviewData = require('./Data/Reviews.json')
// const moviesInDB = require('./src/models/moviesSchema')
// const reviewInDB = require('./src/models/reviewsSchema')
// const connectToDatabase = require('./src/config/db.config')
// const validateMovieData = require('./src/validation/validateMovieEntry')
const movieRouter = require('./src/routes/MovieRoutes')
const reviewRouter = require('./src/routes/ReviewsRouter')

const app = express();
app.use(express.json());
app.use(cors());

let corsOptions = {origin: "http://localhost:3000"};
app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));



// function uploadData(){
//     connectToDatabase()
//     .then(() => {

//       rawReviewData.forEach(element => {

//     const reviewsDocument = new reviewInDB(element)
//     reviewsDocument.save();
//     })
// })
// }

// uploadData()


app.use("/api/movies", movieRouter)

app.use("/api/reviews", reviewRouter)



app.listen(process.env.PORT, () => {
    console.log(`Server is running on port ${process.env.PORT}.`);
  });
  