const Joi = require('joi')
// const { default: mongoose } = require('mongoose')

module.exports =  function validateMoviesData(movie){
    const schema = {
    _id: Joi.optional(),
    Name: Joi.string().min(1).max(50).required(),
    Rating: Joi.number().min(1).max(10).precision(1).required(),
    Certificate: Joi.string().min(1).max(3).required(),
    Released_Year: Joi.number().integer().min(1900).max(2022).required(),
    Runtime: Joi.string().max(20).required(),
    Cast: Joi.string().max(300).required(),
    Genre: Joi.string().max(50).required(),
    Description: Joi.string().max(500).required(),
    Poster: Joi.optional().allow(""),
    __v: Joi.optional()
    }

    return Joi.validate(movie.toObject(), schema)
}