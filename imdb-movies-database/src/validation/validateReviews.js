const Joi = require('joi')
// const { default: mongoose } = require('mongoose')

module.exports =  function validateMoviesData(movie){
    const schema = {
    "_id": Joi.optional(),
    "Name": Joi.string().min(1).max(50).required(),
    "Review": Joi.string().min(3).max(300).required(),
    "MovieId": Joi.string().required(),
    "__v": Joi.optional()
    }

    return Joi.validate(movie, schema)
}