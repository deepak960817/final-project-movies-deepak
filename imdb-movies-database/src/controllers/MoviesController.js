const connectToDatabase = require('../config/db.config')
const validateMovieData = require('../validation/validateMovieEntry')
const validateAtUpdation = require('../validation/validateAtUpdation')
const moviesInDB = require('../models/moviesSchema')
const movieUpdation = require('../validation/movieUpdation')


module.exports.getMovies = async (req,res) => {
    try{
        await connectToDatabase()
        let showMovies;
        showMovies = await moviesInDB.find({})
        
        res.json(showMovies);
    } catch(err){
        res.status(400).json(`Error: ${err.message}`)
    }
}


module.exports.getMoviesById = async (req,res) => {
    try{
        let movieID = req.params.movieId;
        await connectToDatabase();
        const movieFound = await moviesInDB.findOne({_id: movieID});
        if(movieFound)
        {
            res.status(200).json(movieFound)
        }
        else
        {
            res.status(200).json(`No match found with ${movieID} match id.`)
        }
    }catch(err){
        res.status(400).json(`Error: ${err.message}`)
    }
}


module.exports.addMovie = async (req,res) => {
    try{
        const result = validateMovieData(req.body)
        await connectToDatabase();
        if(result.error){
            res.status(400).json(`Error: ${result.error.details[0].message}`)
            return;
        }
        const alreadyExists = await moviesInDB.findOne({Name: result.value.Name});
        if(alreadyExists)
        {
            return res.status(200).json(`Error: Movie with name ${result.value.Name} already exists in database`)
        }
        const newMovieData = await moviesInDB.create(result.value);
    
        res.status(200).json(newMovieData)
    }catch(err){
        res.status(400).json(`Error: ${err.message}`)
    }
}


module.exports.updateMovie = async (req, res) => {
    try{
        let movieID = req.params.movieId;
        await connectToDatabase();
        const movieFound = await moviesInDB.findOne({_id: movieID})
        if(movieFound)
        {
            const tempraryUpdate = movieUpdation(movieFound, req.body)
            const result = validateAtUpdation(tempraryUpdate)
            if(result.error){
                return res.status(400).send(result.error.details[0].message)
            }
            await moviesInDB.findByIdAndUpdate({_id: movieID}, result.value)
            const movieAfterUpdation = await moviesInDB.findOne({_id: movieID})
            res.status(200).json(movieAfterUpdation)
        }
        else
        {
            res.status(200).json("Error: Could not find movie")
        }
    }catch(err){
        res.status(400).json(`Error: ${err.message}`)
    }
}



module.exports.deleteMovie = async (req, res) => {
    try{
        let movieID = req.params.movieId;
        await connectToDatabase();
        const movieFound = await moviesInDB.findByIdAndRemove({_id:movieID})
        if(movieFound)
        {
            res.status(200).json(movieFound)
        }
        else
        {
            res.status(200).json("Error: Could not find movie")
        }
    }catch(err){
        res.status(400).json(`Error: ${err.message}`)
    }
}