const connectToDatabase = require('../config/db.config')
const validateReview = require('../validation/validateReviews')
const reviewInDB = require('../models/reviewsSchema')


module.exports.getReviewsByMovieId = async (req,res) => {
    try{
        let movieId = req.params.movieId;
        await connectToDatabase();
        const foundReviewsByMovieId = await reviewInDB.find({MovieId: movieId})
        if(foundReviewsByMovieId)
        {
            res.status(200).json(foundReviewsByMovieId)
        }
        else
        {
            res.status(200).json(`Error: No reviews found for this movie`)
        }
    }catch(err){
        res.status(400).json(`Error: ${err.message}`)
    }
}


module.exports.addReview = async (req,res) => {
    try{
        const result = validateReview(req.body);
        await connectToDatabase();
        if(result.error){
            return res.status(400).json(`Error: ${result.error.details[0].message}`)
        }
        const newReview = await reviewInDB.create(result.value);
        res.status(200).json(newReview)
    }catch(err){
        res.status(400).json(`Error: ${err.message}`)
    }
}


module.exports.deleteReview = async (req,res) => {
    try{
        let reviewID = req.params.reviewId;
        await connectToDatabase();
        const reviewFound = await reviewInDB.findByIdAndRemove({_id:reviewID})
        if(reviewFound)
        {
            res.status(200).json(reviewFound)
        }
        else
        {
            res.status(200).json(`Error: Could not find review`)
        }
    }catch(err){
        res.status(400).json(`Error: ${err.message}`)
    }
}