const express = require('express');
const router = express.Router();

const moviesController = require('../controllers/MoviesController')


router.get("/", moviesController.getMovies)

router.get("/:movieId", moviesController.getMoviesById)

router.post("/", moviesController.addMovie)

router.put("/:movieId", moviesController.updateMovie)

router.delete("/:movieId", moviesController.deleteMovie)

module.exports = router;
