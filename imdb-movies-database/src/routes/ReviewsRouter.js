const express = require('express');
const router = express.Router();

const reviewsController = require('../controllers/ReviewsController')

router.get("/:movieId", reviewsController.getReviewsByMovieId)

router.post("/", reviewsController.addReview)

router.delete("/:reviewId", reviewsController.deleteReview)

module.exports = router;