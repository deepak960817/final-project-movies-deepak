const number = require('joi/lib/types/number')
const mongoose = require('mongoose')

const moviesSchema = mongoose.Schema({
    "Name": String,
    "Rating": Number,
    "Certificate": String,
    "Released_Year": Number,
    "Runtime": String,
    "Cast": String,
    "Genre": String,
    "Description": String,
    "Poster": String,
})

module.exports = mongoose.model("Movies", moviesSchema)