const mongoose = require('mongoose')

const reviewsSchema = mongoose.Schema({
    "Name": String,
    "Review": String,
    "MovieId": mongoose.Types.ObjectId
})

module.exports = mongoose.model("Reviews", reviewsSchema)