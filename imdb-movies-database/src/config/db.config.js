const mongoose  = require('mongoose')

module.exports = async function() {
    try{
        await mongoose.connect(process.env.MONGODB_URI),
        {useNewUrlParser: true,
        useUnifiedTopology: true}
        console.log("Connected to Database")
    } catch(err) {
        console.error("Failed to Connect to Database");
    }
}

