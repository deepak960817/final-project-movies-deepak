import { ActionType } from "../actionTypes/actionTypes";

export const getMovies = (movies) => {
    return {
        type: ActionType.GET_MOVIES,
        payload: movies
    }
}

export const getMovieById = (movie) => {
    return {
        type: ActionType.SELECTED_MOVIE,
        payload: movie
    }
}

export const addMovies = (movie) => {
    return{
        type: ActionType.ADD_MOVIE,
        payload: movie
    }
}

export const deleteMovies = (id) => {
    return{
        type: ActionType.DELETE_MOVIE,
        payload: id
    }
}

export const selectedMovie = (movie) => {
    return{
        type: ActionType.SELECTED_MOVIE,
        payload: movie
    }
}

export const getReview = (review) => {
    return{
        type: ActionType.GET_REVIEW_BY_MOVIE_ID,
        payload: review
    }
}

export const addReview = (review) => {
    return {
        type: ActionType.ADD_REVIEW,
        payload:review
    }
}

export const deleteReview = (id) => {
    return{
        type: ActionType.DELETE_REVIEW,
        payload:id
    }
}