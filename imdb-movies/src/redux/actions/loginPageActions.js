import { ActionType } from "../actionTypes/actionTypes";

export const setUser = (user) => {
    return {
        type: ActionType.SET_USER,
        payload: user
    }
}

export const setEmail = (email) => {
    return {
        type: ActionType.SET_EMAIL,
        payload: email
    }
}

export const setPassword = (password) => {
    return {
        type: ActionType.SET_PASSWORD,
        payload: password
    }
}

export const setEmailError = (error) => {
    return {
        type: ActionType.SET_EMAIL_ERROR,
        payload: error
    }
}

export const setPasswordError = (error) => {
    return {
        type: ActionType.SET_PASSWORD_ERROR,
        payload: error
    }
}

export const setHasAccount = (account) => {
    return {
        type: ActionType.SET_HAS_ACCOUNT,
        payload: account
    }
}

export const setLogout = (data) => {
    return {
        type: ActionType.SET_LOGOUT,
        payload: data
    }
}
