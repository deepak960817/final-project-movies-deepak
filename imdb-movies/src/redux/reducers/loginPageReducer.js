import { ActionType } from "../actionTypes/actionTypes";

const initialState = {
    user: "",
    email: "",
    password: "",
    emailError: "",
    passwordError: "",
    hasAccount: false,
    logout: ""
}

export const loginPageReducer = (state = initialState, {type, payload}) => {

    switch(type){
        case ActionType.SET_USER:
            return{
                ...state,
                user: payload
            }
        
        case ActionType.SET_EMAIL:
            return{
                ...state,
                email: payload
            }

        case ActionType.SET_PASSWORD:
            return{
                ...state,
                password: payload
            }

        case ActionType.SET_EMAIL_ERROR:
            return{
                ...state,
                emailError: payload
            }

        case ActionType.SET_PASSWORD_ERROR:
            return{
                ...state,
                passwordError: payload
            }

        case ActionType.SET_HAS_ACCOUNT:
            return{
                ...state,
                hasAccount: payload
            }

        case ActionType.SET_LOGOUT:
            return{
                ...state,
                logout: payload
            }

        default:
            return state
    }
}