import { ActionType } from "../actionTypes/actionTypes";

const initialState = {
    moviesData: [],
    currentMovie:[],
    reviewByMovie:[]
}

export const moviesReducer = (state = initialState, {type, payload}) => {
    
    switch(type){
        case ActionType.GET_MOVIES:
            return{
                ...state,
                moviesData: payload,
            }
        
        case ActionType.SELECTED_MOVIE:
            return{
                ...state,
                currentMovie: payload
            }

        
        case ActionType.ADD_MOVIE:
            return{
                ...state,
                moviesData: [payload, ...state.moviesData]
            }

        case ActionType.DELETE_MOVIE:
            return{
                ...state,
                moviesData: [...state.moviesData.filter(val => val._id !== payload)]
            }
        
        case ActionType.GET_REVIEW_BY_MOVIE_ID:
            return{
                ...state,
                reviewByMovie: payload
            }

        case ActionType.ADD_REVIEW:
            return{
                ...state,
                reviewByMovie: [...state.reviewByMovie, payload]
            }

        case ActionType.DELETE_REVIEW:
            return{
                ...state,
                reviewByMovie: [...state.reviewByMovie.filter(val => val._id !== payload)]
            }

        default:
            return state;
    }    
}