import { combineReducers } from "redux";
import { moviesReducer } from "./moviesReducer";
import { loginPageReducer } from "./loginPageReducer";

const reducers = combineReducers({
    moviesInfo : moviesReducer,
    loginInfo : loginPageReducer
})

export default reducers;