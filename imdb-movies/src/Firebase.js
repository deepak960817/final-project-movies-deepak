import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';

const firebaseConfig = {
    apiKey: "AIzaSyDiFoiz1WIeoSRze9PmTKy-mlaW5O9M6PA",
    authDomain: "movie-glance.firebaseapp.com",
    projectId: "movie-glance",
    storageBucket: "movie-glance.appspot.com",
    messagingSenderId: "783237506908",
    appId: "1:783237506908:web:a53489e2d31499a125c399"
  };
  
  const firebaseApp = firebase.initializeApp(firebaseConfig);

  export default firebaseApp;
  