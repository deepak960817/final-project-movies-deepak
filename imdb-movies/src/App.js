import React, { Component } from 'react';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import {connect} from 'react-redux'

import './App.css';
import HomePage from './components/HomePage';
import NavBar from './components/NavBar';
import SideBar from './components/SideBar';
import MoviePage from './components/MoviePage';
import MoviePageSideBar from './components/moviePageSideBar';
import LoginPage from './components/loginPage';

const mapStateToProps = ((state) => {
  return{
      allMovies: state.moviesInfo.moviesData,
      currentMovie: state.moviesInfo.currentMovie,
      user: state.loginInfo.user,
      logout: state.loginInfo.logout
  }
})

class App extends Component {


  fetchBoardID = () => { 
    return window.location.pathname.split("/")[window.location.pathname.split("/").length-1];
  }


  render() { 
    return ( 
      <div className="App">
        <Router>
        <Routes>
        {
          this.props.user === "" ?
          <Route exact path={"/"} element={
            <LoginPage />
          }/>
          : 
          <>
           <Route exact path={"/"} element={
            <>
            <NavBar userName={this.props.user.multiFactor.user.email} />
            <SideBar />
            <HomePage />
            </>
          }/>
          <Route path={`/:${this.fetchBoardID()}`} element={
            <>
            <NavBar userName={this.props.user.multiFactor.user.email} />
            <MoviePageSideBar />
            <MoviePage />
            </>
          }/>
          </>
        }
        </Routes>
      </Router>
    </div>
    );
  }
}
 
export default connect(mapStateToProps, {})(App);
