import React, { Component } from 'react';
import {connect} from 'react-redux'

import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import {Link} from 'react-router-dom';

import img from '../Data/cinema.jpg'

const mapStateToProps = ((state) => {
    return{
        currentMovie: state.moviesInfo.currentMovie,
        logout: state.loginInfo.logout
    }
})


class NavBar extends Component {
    state = {  } 

    render() {
         
        return (
            <Navbar sticky="top" variant="dark" style={{height:'20vh', opacity:'1', backgroundPositionY:'25%, center', backgroundSize:'100% 100%' , backgroundImage: `url(${img})`, display:'flex', justifyContent:'space-between'}}>
                <Container style={{display:'flex', alignItems: 'center', justifyContent:'center'}}>
                <Navbar.Brand style={{fontSize:'5rem', fontWeight:'bold', padding:'1rem 0 0 20rem' }}>MOVIE GLANCE</Navbar.Brand>
                </Container>
                <span style={{marginRight:'3rem'}}>
                    <p style={{color:'gray'}}>{`Hello, ${this.props.userName}`}</p>
                    <Link to={"/"}>
                    <Button variant="danger" style={{marginLeft:'5rem'}} onClick={()=> this.props.logout()}>Logout</Button>
                    </Link>
                </span>
                
            </Navbar>
        );
    }
}
 
export default connect(mapStateToProps, {})(NavBar);