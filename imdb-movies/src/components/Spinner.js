import React from 'react';
import Spinner from 'react-bootstrap/Spinner';

const SpinnerLoading = () => {
    return ( 
        <Spinner animation="border" role="status" variant="light" style={{height:'5rem', width:'5rem'}}></Spinner>
     );
}
 
export default SpinnerLoading;