import React, { Component } from 'react';
import {connect} from 'react-redux'

import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import * as APICalls from './APICalls';
import * as actions from '../redux/actions/moviesAction'

const mapStateToProps = ((state) => {
    return{
        currentMovie: state.moviesInfo.currentMovie,
    }
})

const mapDispatchToProps = ((dispatch) => {
    return{
        selectMovie: (data) => dispatch(actions.selectedMovie(data)),
    }
})


let name, value;
class UpdateMovieModal extends Component {
    state = { 
        show: false,
        setShow: false,
        updateMovie: {Name: "", Rating: 0, Certificate: "", Released_Year: 0, Runtime: "", Cast: "", Genre: "", Description: "", Poster: ""},
        smShow: false,
        setSmShow: false,
        updateMovieResponse:""
    }

    handleClose = () => this.setState({show: false, setShow: false});
    handleShow = () => this.setState({show: true, setShow: true});

    handleCloseSm = () => this.setState({smShow: false, setSmShow: false});
    handleShowSm = () => this.setState({smShow: true, setSmShow: true});

    handleInputChange = (e) => {
        name = e.target.name;
        value = e.target.value;

        this.setState({updateMovie: {...this.state.updateMovie, [name]: value}})
    }

    handleSubmitToUpdateMovie = (e) => {
        e.preventDefault();
        APICalls.updateMovie(this.props.movieData._id, this.state.updateMovie).then((result) => {
        
                if(typeof result === "string")
                {
                    this.setState({updateMovieResponse: result}, () => {
                    setTimeout(() => {
                        this.handleCloseSm();
                    },3000)
                    this.handleShowSm();
                })}
                else
                {
                    this.setState({updateMovieResponse: "Success: Movie Updated Successfully"}, () => {
                    setTimeout(() => {
                        this.handleCloseSm();
                    },3000)
                    this.handleClose();
                    this.handleShowSm();
                    this.props.selectMovie(result)
                })}
        }) 
    }

    render() { 
        
        return (
            <div>
            <Button variant='warning' style={{margin:'5rem 2rem 0 2rem'}} onClick={() => {this.handleShow()}}>{this.props.buttonName}</Button>
            <Modal show={this.state.show} onHide={() => {this.setState({checklistShow: false, show: false})}} size="lg" aria-labelledby="example-modal-sizes-title-lg">
            <Modal.Header closeButton>
            <Modal.Title>{this.props.modalTitle}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label><b>{this.props.modalDescription}</b></Form.Label>
                        <div style={{display:'flex', alignItems:'center', gap:'1rem', marginTop:'2rem'}}>
                            <h6 style={{width:'8.5rem'}}>Name:</h6>
                            <Form.Control type="text" name="Name" autoComplete='off' placeholder={this.props.currentMovie.Name} style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)}/>
                        </div>
                        <div style={{display:'flex', alignItems:'center', gap:'1rem'}}>
                            <h6 style={{width:'8.5rem'}}>Rating:</h6>
                            <Form.Control type="text" name="Rating" autoComplete='off' placeholder={this.props.currentMovie.Rating} style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)}/>
                        </div>
                        <div style={{display:'flex', alignItems:'center', gap:'1rem'}}>
                            <h6 style={{width:'8.5rem'}}>Certificate:</h6>
                            <Form.Control type="text" name="Certificate" autoComplete='off' placeholder={this.props.currentMovie.Certificate} style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)}/>
                        </div>
                        <div style={{display:'flex', alignItems:'center', gap:'1rem'}}>
                            <h6 style={{width:'8.5rem'}}>Released_Year:</h6>
                            <Form.Control type="text" name="Released_YEar" autoComplete='off' placeholder={this.props.currentMovie.Released_Year} style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)}/>
                        </div>
                        <div style={{display:'flex', alignItems:'center', gap:'1rem'}}>
                            <h6 style={{width:'8.5rem'}}>Runtime:</h6>
                            <Form.Control type="text" name="Runtime" autoComplete='off' placeholder={this.props.currentMovie.Runtime} style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)}/>
                        </div>
                        <div style={{display:'flex', alignItems:'center', gap:'1rem'}}>
                            <h6 style={{width:'8.5rem'}}>Cast:</h6>
                            <Form.Control type="text" name="Cast" autoComplete='off' placeholder={this.props.currentMovie.Cast} style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)}/>
                        </div>
                        <div style={{display:'flex', alignItems:'center', gap:'1rem'}}>
                            <h6 style={{width:'8.5rem'}}>Genre:</h6>
                            <Form.Control type="text" name="Genre" autoComplete='off' placeholder={this.props.currentMovie.Genre} style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)}/>
                        </div>
                        <div style={{display:'flex', alignItems:'center', gap:'1rem'}}>
                            <h6 style={{width:'8.5rem'}}>Description:</h6>
                            <Form.Control type="text" name="Description" autoComplete='off' placeholder={this.props.currentMovie.Description} style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)}/>
                        </div>
                        <div style={{display:'flex', alignItems:'center', gap:'1rem'}}>
                            <h6 style={{width:'8.5rem'}}>Poster Link:</h6>
                            <Form.Control type="text" name="Poster" autoComplete='off' placeholder={this.props.currentMovie.Poster} style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)} />
                        </div>
                    </Form.Group>
                    <div style={{display:'flex', justifyContent:'flex-end'}}>
                        <Button variant="primary" type="submit" onClick={(e) => {this.handleSubmitToUpdateMovie(e); this.handleClose()}}>Submit</Button>
                    </div>
                    </Form>
                </Modal.Body>
                </Modal>
                <>
                    <Modal size="sm" show={this.state.smShow} onHide={() => this.setState({smShow: false})} aria-labelledby="example-modal-sizes-title-sm">
                    <Modal.Header closeButton>
                    <Modal.Title id="example-modal-sizes-title-sm">Message</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{this.state.updateMovieResponse}</Modal.Body>
                    </Modal>
                </>
                </div>
        );
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(UpdateMovieModal);