import React, { Component } from 'react';
import {connect} from 'react-redux'

import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import * as APICalls from './APICalls';
import * as actions from '../redux/actions/moviesAction'

const mapStateToProps = ((state) => {
    return{
        allMovies: state.moviesInfo.moviesData,
    }
})

const mapDispatchToProps = ((dispatch) => {
    return{
        addMovies: (data) => dispatch(actions.addMovies(data)),
        selectMovie: (data) => dispatch(actions.selectedMovie(data)),
    }
})


let name, value;
class AddMovieModal extends Component {
    state = { 
        show: false,
        setShow: false,
        parameterValue: "",
        newMovie: {Name: "", Rating: 0, Certificate: "", Released_Year: 0, Runtime: "", Cast: "", Genre: "", Description: "", Poster: ""},
        smShow: false,
        setSmShow: false,
        addMovieResponse:""
     }

    handleClose = () => this.setState({show: false, setShow: false});
    handleShow = () => this.setState({show: true, setShow: true});

    handleCloseSm = () => this.setState({smShow: false, setSmShow: false});
    handleShowSm = () => this.setState({smShow: true, setSmShow: true});

    handleInputChange = (e) => {
        name = e.target.name;
        value = e.target.value;

        this.setState({newMovie: {...this.state.newMovie, [name]: value}})
    }

    handleSubmitToAddMovie = (e) => {
        e.preventDefault();
        APICalls.addMovie(this.state.newMovie).then((result) => {
                if(typeof result === "string")
                {
                    this.setState({addMovieResponse: result}, () => {
                    setTimeout(() => {
                        this.handleCloseSm();
                    },3000)
                    this.handleShowSm();
                })}
                else
                {
                    this.setState({addMovieResponse: "Success: Movie Added Successfully"}, () => {
                    setTimeout(() => {
                        this.handleCloseSm();
                    },3000)
                    this.handleClose();
                    this.handleShowSm();
                    this.props.addMovies(result)
                })}
            })
    }


    render() { 
        
        return ( 
            <div>
                <Button variant='warning' style={{margin:'5rem 2rem 0 2rem'}} onClick={() => {this.handleShow()}}>{this.props.buttonName}</Button>
                <Modal show={this.state.show} onHide={() => {this.setState({checklistShow: false, show: false})}} size="lg" aria-labelledby="example-modal-sizes-title-lg">
                <Modal.Header closeButton>
                <Modal.Title>{this.props.modalTitle}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label><b>{this.props.modalDescription}</b></Form.Label>
                            <div style={{display:'flex', alignItems:'center', gap:'1rem'}}>
                                <h6 style={{width:'8.5rem'}}>Name:</h6>
                                <Form.Control type="text" name="Name" placeholder='Movie Name' autoComplete='off' style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)}/>
                            </div>
                            <div style={{display:'flex', alignItems:'center', gap:'1rem'}}>
                                <h6 style={{width:'8.5rem'}}>Rating:</h6>
                                <Form.Control type="text" name="Rating" placeholder="Rating" autoComplete='off' style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)}/>
                            </div>
                            <div style={{display:'flex', alignItems:'center', gap:'1rem'}}>
                                <h6 style={{width:'8.5rem'}}>Certificate:</h6>
                                <Form.Control type="text" name="Certificate" placeholder="Certificate" autoComplete='off' style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)}/>
                            </div>
                            <div style={{display:'flex', alignItems:'center', gap:'1rem'}}>
                                <h6 style={{width:'8.5rem'}}>Released Year:</h6>
                                <Form.Control type="text" name="Released_Year" placeholder="Released_Year" autoComplete='off' style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)}/>
                            </div>
                            <div style={{display:'flex', alignItems:'center', gap:'1rem'}}>
                                <h6 style={{width:'8.5rem'}}>Runtime:</h6>
                                <Form.Control type="text" name="Runtime" placeholder="Runtime" autoComplete='off' style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)}/>
                            </div>
                            <div style={{display:'flex', alignItems:'center', gap:'1rem'}}>
                                <h6 style={{width:'8.5rem'}}>Cast:</h6>
                                <Form.Control type="text" name="Cast" placeholder="Cast" autoComplete='off' style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)}/>
                            </div>
                            <div style={{display:'flex', alignItems:'center', gap:'1rem'}}>
                                <h6 style={{width:'8.5rem'}}>Genre:</h6>
                                <Form.Control type="text" name="Genre" placeholder="Genre" autoComplete='off' style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)}/>
                            </div>
                            <div style={{display:'flex', alignItems:'center', gap:'1rem'}}>
                                <h6 style={{width:'8.5rem'}}>Description:</h6>
                                <Form.Control type="text" name="Description" placeholder="Description" autoComplete='off' style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)}/>
                            </div>
                            <div style={{display:'flex', alignItems:'center', gap:'1rem'}}>
                                <h6 style={{width:'8.5rem'}}>Poster Link:</h6>
                                <Form.Control type="text" name="Poster" placeholder="Poster link if any" autoComplete='off' style={{margin:'0.5rem 0 0.5rem 0'}} onChange={(e) => this.handleInputChange(e)} />
                            </div>
                        </Form.Group>
                        <div style={{display:'flex', justifyContent:'flex-end'}}>
                            <Button variant="primary" type="submit" onClick={(e) => {this.handleSubmitToAddMovie(e)}}>Submit</Button>
                        </div>
                    </Form>
                </Modal.Body>
                </Modal>
                <>
                    <Modal size="sm" show={this.state.smShow} onHide={() => this.setState({smShow: false})} aria-labelledby="example-modal-sizes-title-sm">
                    <Modal.Header closeButton>
                    <Modal.Title id="example-modal-sizes-title-sm">Message</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{this.state.addMovieResponse}</Modal.Body>
                    </Modal>
                </>
            </div>
        );
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(AddMovieModal);
