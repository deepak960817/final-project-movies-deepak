import axios from 'axios';

export function getMovies() {
    return axios.get('http://localhost:5000/api/movies')
    .then((result) => result.data)
    .catch((err) => {
        if (err.response) {
            return (err.response.data)
        } else if (err.request) {
            return ("Request Error: Invalid Request")
        } else {
            return ("Error: Unable to Serve")
        }  
    })
}



export function getMoviebyId(movieId) {
    return axios.get(`http://localhost:5000/api/movies/${movieId}`)
    .then((result) => result.data)
    .catch((err) => {
        if (err.response) {
            return (err.response.data)
        } else if (err.request) {
            return ("Request Error: Invalid Request")
        } else {
            return ("Error: Unable to Serve")
        }  
    })
}




export function addMovie(movie) {
    return axios.post('http://localhost:5000/api/movies', 
       movie
    )
    .then((result) => result.data)
    .catch((err) => {
        if (err.response) {
            return (err.response.data)
        } else if (err.request) {
            return ("Request Error: Invalid Request")
        } else {
            return ("Error: Unable to Serve")
        }  
    })
}



export function updateMovie(movieId, movieInfo) {
 
    return axios.put(`http://localhost:5000/api/movies/${movieId}`, 
        movieInfo
    )
    .then((result) => result.data)
    .catch((err) => {
        if (err.response) {
            return (err.response.data)
        } else if (err.request) {
            return ("Request Error: Invalid Request")
        } else {
            return ("Error: Unable to Serve")
        }  
    })
}



export function deleteMovie(movieId) {
    return axios.delete(`http://localhost:5000/api/movies/${movieId}`)
    .then((result) => result.data)
    .catch((err) => {
        if (err.response) {
            return (err.response.data)
        } else if (err.request) {
            return ("Request Error: Invalid Request")
        } else {
            return ("Error: Unable to Serve")
        }  
    })
}


export function getReviewsByMovieId(movieId) {
    return axios.get(`http://localhost:5000/api/reviews/${movieId}`)
    .then((result) => result.data)
    .catch((err) => {
        if (err.response) {
            return (err.response.data)
        } else if (err.request) {
            return ("Request Error: Invalid Request")
        } else {
            return ("Error: Unable to Serve")
        }  
    })
}


export function addReviews(review){
    return axios.post(`http://localhost:5000/api/reviews`, review)
    .then((result) => result.data)
    .catch((err) => {
        if (err.response) {
            return (err.response.data)
        } else if (err.request) {
            return ("Request Error: Invalid Request")
        } else {
            return ("Error: Unable to Serve")
        }  
    })
}


export function deleteReviews(reviewId){
    return axios.delete(`http://localhost:5000/api/reviews/${reviewId}`)
    .then((result) => result.data)
    .catch((err) => {
        if (err.response) {
            return (err.response.data)
        } else if (err.request) {
            return ("Request Error: Invalid Request")
        } else {
            return ("Error: Unable to Serve")
        }  
    })
}