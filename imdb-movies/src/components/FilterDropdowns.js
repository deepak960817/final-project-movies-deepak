import React, {Component} from 'react';
import {connect} from 'react-redux';
import Form from 'react-bootstrap/Form';
import * as actions from '../redux/actions/moviesAction'

const mapStateToProps = ((state) => {
    return{
        allMovies: state.moviesInfo.moviesData,
    }
})

const mapDispatchToProps = ((dispatch) => {
    return{
        getMovies: (data) => dispatch(actions.getMovies(data)),
        selectMovie: (data) => dispatch(actions.selectedMovie(data)),
    }
})

class FilterDropDowns extends Component {
    state = {
        moviesList: this.props.allMovies,
        searchBarValue: "",
    }


    handleGenre(value){
        if(value !== "Genre"){
            const filterByGenre = this.state.moviesList.filter((element) => {
                return(element.Genre.includes(value))
            })
            this.props.getMovies(filterByGenre);
        }
        else
        {
            this.props.getMovies(this.state.moviesList)
        }
    }

    handleRating(value){
        if(value !== "Rating"){
            const filterByRating = this.state.moviesList.filter((element) => {
                return(element.Rating >= (value.substring(0,1)) && element.Rating < (value.substring(5)))
            })
            this.props.getMovies(filterByRating);
        }
        else
        {
            this.props.getMovies(this.state.moviesList)
        }
    }

    handleYear(value){
        if(value !== "Release Year"){
            const filterByYear = this.state.moviesList.filter((element) => {
                return(element.Released_Year >= (value.substring(0,4)) && element.Rating < (value.substring(7)))
            })
            this.props.getMovies(filterByYear);
        }
        else
        {
            this.props.getMovies(this.state.moviesList)
        }
    }

    handleInputChange =() => {
        const validMovies = this.state.moviesList.filter((val) => {
            return (this.state.searchBarValue === "" || val.Name.toLowerCase().includes(this.state.searchBarValue.toLowerCase()));
        })
        this.props.getMovies(validMovies);
    }


    render() { 
        return (
            <div>
             <Form className="d-flex" style={{margin:'2rem 0 3rem 4rem'}}>
                <Form.Control type="search" placeholder="Search Movie..." className="me-2" aria-label="Search" style={{width:'75%'}} onChange={(e) => {this.setState({searchBarValue: e.target.value}, () => {this.handleInputChange()})}}/>
            </Form>
            <h3 style={{color:'white'}}>Filter By</h3>
            <select onChange={(e) => this.handleGenre(e.target.value)} className='filterBox' type="text" style={{width: '60%', padding:'0.5rem', borderRadius:'0.5rem', backgroundColor:'white', margin:'0.5rem 0 0.5rem 0'}}>
                <option>Genre</option>
                <option>Action</option>
                <option>Drama</option>
                <option>History</option>
                <option>Crime</option>
                <option>Adventure</option>
                <option>Mystery</option>
                <option>War</option>
                <option>Biography</option>
                <option>Sci-Fi</option>
                <option>Fantasy</option>
            </select>
            <select onChange={(e) => this.handleRating(e.target.value)} className='filterBox' type="text" style={{width: '60%', padding:'0.5rem', borderRadius:'0.5rem', backgroundColor:'white', margin:'0.5rem 0 0.5rem 0'}}>
                <option>Rating</option>
                <option>9 to 10</option>
                <option>8 to 9</option>
                <option>7 to 8</option>
                <option>6 to 7</option>
                <option>5 to 6</option>
                <option>4 to 5</option>
                <option>3 to 4</option>
                <option>2 to 3</option>
                <option>1 to 2</option>
            </select>
            <select onChange={(e) => this.handleYear(e.target.value)} className='filterBox' type="text" style={{width: '60%', padding:'0.5rem', borderRadius:'0.5rem', backgroundColor:'white', margin:'0.5rem 0 0.5rem 0'}}>
                <option>Release Year</option>
                <option>2021 - 2023</option>
                <option>2011 - 2020</option>
                <option>2001 - 2010</option>
                <option>1991 - 2000</option>
                <option>1981 - 1990</option>
                <option>1971 - 1980</option>
                <option>1961 - 1970</option>
                <option>1951 - 1960</option>
            </select>
            
            </div>
     )
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(FilterDropDowns);