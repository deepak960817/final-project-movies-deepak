import React, { Component } from 'react';
import {connect} from 'react-redux'

import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import * as APICalls from './APICalls';
import * as actions from '../redux/actions/moviesAction'

const mapStateToProps = ((state) => {
    return{
        currentMovie: state.moviesInfo.currentMovie,
    }
})

const mapDispatchToProps = ((dispatch) => {
    return{
        deleteMovies: (data) => dispatch(actions.deleteMovies(data)),
        selectMovie: (data) => dispatch(actions.selectedMovie(data))
    }
})


class DeleteMovieModal extends Component {
    state = { 
        show: false,
        setShow: false,
        smShow: false,
        setSmShow: false,
        deleteMovieResponse:""
     }

    handleClose = () => this.setState({show: false, setShow: false});
    handleShow = () => this.setState({show: true, setShow: true});

    handleCloseSm = () => this.setState({smShow: false, setSmShow: false});
    handleShowSm = () => this.setState({smShow: true, setSmShow: true});

    handleSubmitToDeleteMovie = (e) => {
        e.preventDefault()
        APICalls.deleteMovie(this.props.movieId).then(result => {
                if(typeof result === "string")
                {
                    this.setState({deleteMovieResponse: result}, () => {
                    setTimeout(() => {
                        this.handleCloseSm();
                    },2000)
                    this.handleShowSm();
                })}
                else
                {
                    this.setState({deleteMovieResponse: "Success: Movie Deleted Successfully"}, () => {
                    setTimeout(() => {
                        this.handleCloseSm();
                    },2000)
                    this.handleClose();
                    this.handleShowSm();
                    this.props.deleteMovies(this.props.movieId)
                    this.props.selectMovie([])
                })}
            }
        )
    }
        
    render() { 
        // console.log(this.state.deleteMovieResponse)
        return (
            <div>
                <Button variant='danger' style={{margin:'5rem 2rem 0 2rem'}} onClick={() => {this.handleShow()}}>Delete Movie</Button>
                <Modal show={this.state.show} onHide={() => this.setState({show: false})} size="sm" aria-labelledby="example-modal-sizes-title-lg">
                <Modal.Header closeButton>
                <Modal.Title>Delete Movie</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div style={{display:'flex', alignItems:'center'}}>
                        <h6>Are you sure?</h6>
                    </div>
                    <div style={{display:'flex', justifyContent:'flex-end', gap:'1rem'}}>
                        <Button variant="danger" type="submit" onClick={(e) => {this.handleSubmitToDeleteMovie(e); this.handleClose()}}>Yes</Button>
                        <Button variant="primary" type="submit" onClick={() => {this.handleClose()}}>No</Button>
                    </div>
                </Modal.Body>
                </Modal>
                <>
                    <Modal size="sm" show={this.state.smShow} onHide={() => this.setState({smShow: false})} aria-labelledby="example-modal-sizes-title-sm">
                    <Modal.Header closeButton>
                    <Modal.Title id="example-modal-sizes-title-sm">Message</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{this.state.deleteMovieResponse}</Modal.Body>
                    </Modal>
                </>
                </div>
        );
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(DeleteMovieModal);