import React, { Component } from 'react';
import {connect} from 'react-redux'

import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

import * as actions from '../redux/actions/loginPageActions'
import fire from '../Firebase'
import loginPageImage from '../Data/loginPagePic.jpg'
// import HomePage from './HomePage';
// import NavBar from './NavBar';
// import SideBar from './SideBar';

const mapStateToProps = ((state) => {
    return{
        user: state.loginInfo.user,
        email: state.loginInfo.email,
        password: state.loginInfo.password,
        emailError: state.loginInfo.emailError,
        passwordError: state.loginInfo.passwordError,
        hasAccount: state.loginInfo.hasAccount,
        logout: state.loginInfo.logout
    }
})

const mapDispatchToProps = ((dispatch) => {
    return{
        setUser: (data) => dispatch(actions.setUser(data)),
        setEmail: (data) => dispatch(actions.setEmail(data)),
        setPassword: (data) => dispatch(actions.setPassword(data)),
        setEmailError: (data) => dispatch(actions.setEmailError(data)),
        setPasswordError: (data) => dispatch(actions.setPasswordError(data)),
        setHasAccount: (data) => dispatch(actions.setHasAccount(data)),
        setLogout: (data) => dispatch(actions.setLogout(data)) 
    }
})

class loginPage extends Component {

    clearInput = () => {
        this.props.setEmail("")
        this.props.setPassword("")
    }

    clearErrors = () => {
        this.props.setEmailError("")
        this.props.setPasswordError("")
    }


    handleLogin = () => {
        this.clearErrors()
        fire.auth()
            .signInWithEmailAndPassword(this.props.email, this.props.password)
            .catch(err => {
                switch(err.code){
                    case "auth/invalid-email":
                    case "auth/user-disabled":
                    case "auth/user-not-found":
                        this.props.setEmailError(err.message)
                        break;
                    case "auth/wrong-password":
                        this.props.setPasswordError(err.message)
                        break;
                    default:
                        this.props.setEmailError("")
                        this.props.setPasswordError("")
                }
            })
    }

    handleSignUp = () => {
        this.clearErrors()
        fire.auth()
        .createUserWithEmailAndPassword(this.props.email, this.props.password)
        .catch(err => {
            switch(err.code){
                case "auth/email-already-in-use":
                case "auth/invalid-email":
                    this.props.setEmailError(err.message.split(":")[err.message.split(":").length -1])
                    break;
                case "auth/weak-password":
                    this.props.setPasswordError(err.message.split(":")[err.message.split(":").length -1])
                    break;
                default:
                    this.props.setEmailError("")
                    this.props.setPasswordError("")
            }
        })
    }

    handleLogout = () => {
        fire.auth().signOut();
    }

    authListener = () => {
        fire.auth().onAuthStateChanged((user) => {
            if(user)
            {
                this.clearInput()
                this.props.setUser(user)
            }
            else
            {
                this.props.setUser("")
            }
        })
    }

    componentDidMount(){
        this.authListener()
        this.props.setLogout(this.handleLogout)
    }


    render() { 
        return ( //this.props.user === "" ?
            <div style={{display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center', height:'100vh', width:'100vw', backgroundImage: `url(${loginPageImage})`, backgroundRepeat:'no-repeat'}}>
                <h3 style={{color:'yellow'}}>Welcome</h3>
                <br></br>
                <h1 style={{fontSize:'5rem', color:'white', fontWeight:'bold', margin:'0 0 5rem 0' }}>MOVIE GLANCE</h1>
                <Card style={{ maxWidth: '18rem', minHeight:'20rem', backgroundColor:'black', color:'white'}}>
                <Card.Body>
                    <Card.Title style={{marginBottom:'2rem'}}>User Login/SignUp</Card.Title>
                    <Form>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <div style={{marginBottom:'1rem', display:'flex', flexDirection:'column', alignItems:'flex-start'}}>
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" placeholder="Email" autoFocus autoComplete='off' value={this.props.email} required onChange={(e) => {this.props.setEmail(e.target.value)}}/>
                            <p style={{color:'yellow'}}>{this.props.emailError}</p>
                        </div>
                        <div style={{marginBottom:'1rem', display:'flex', flexDirection:'column', alignItems:'flex-start'}}>
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" required  value={this.props.password} onChange={(e) => {this.props.setPassword(e.target.value)}}/>
                            <p style={{color:'yellow'}}>{this.props.passwordError}</p>
                        </div>
                        <div className='btnContainer'>
                            { this.props.hasAccount ? (
                                <>
                                <Button variant="primary" style={{marginBottom:'3rem'}} onClick={this.handleLogin}>Sign In</Button>
                                <p>Don't have an account?<span style={{marginLeft:'0.5rem', color:'yellow'}} onClick={() => {this.clearInput(); this.clearErrors(); this.props.setHasAccount(!this.props.hasAccount)}}>Sign Up</span></p>
                                </>
                            ) : (
                                <>
                                <Button variant="danger" style={{marginBottom:'3rem'}} onClick={this.handleSignUp}>Sign Up</Button>
                                <p>Already have an account?<span style={{marginLeft:'0.5rem', color:'yellow'}} onClick={() => {this.clearInput(); this.clearErrors(); this.props.setHasAccount(!this.props.hasAccount)}}>Sign In</span></p>
                                </>
                            )}
                        </div>
                    </Form.Group>
                    </Form>
                </Card.Body>
                </Card>
            </div>
            // :<>
            //     <NavBar userName={this.props.user.multiFactor.user.email} logout={this.handleLogout}/>
            //     <SideBar />
            //     <HomePage />
            // </>
        );
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(loginPage);