import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import sidebarImg from '../Data/imdbAd.png'


class MoviePageSideBar extends Component {

    render() { 

        return (
            <div style={{position:'fixed', height:"100%", ZIndex:'0', top:'20vh', left:'0', overflowX: 'hidden', width:'20vw', backgroundImage: `url(${sidebarImg})`, backgroundSize:'20vw 80vh', display:'flex', flexDirection:'column', alignItems:'center'}}>
                <div style={{ alignItems:'centre', margin:'40rem 5rem 0 0'}}>
                <p>For More, Visit</p><Link to={"https://www.imdb.com"}><span><b>{`imdb.com`}</b></span></Link>
            </div>
            </div>
        );
    }
}
 
export default MoviePageSideBar;