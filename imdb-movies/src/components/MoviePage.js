import React, { Component } from 'react';
import {connect} from 'react-redux'
import {Link} from 'react-router-dom';

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';

import UpdateMovieModal from './UpdateMovieModal';
import DeleteMovieModal from './DeleteMovieModal';
import * as actions from '../redux/actions/moviesAction'
import * as APICalls from './APICalls';

const mapStateToProps = ((state) => {
    return{
        currentMovie: state.moviesInfo.currentMovie,
        reviewByMovie: state.moviesInfo.reviewByMovie
    }
})

const mapDispatchToProps = ((dispatch) => {
    return{
        getMovies: (data) => dispatch(actions.getMovies(data)),
        selectMovie: (data) => dispatch(actions.selectedMovie(data)),
        addReview: (data) => dispatch(actions.addReview(data)),
        deleteReview: (data) => dispatch(actions.deleteReview(data)),
        getReview: (data) => dispatch(actions.getReview(data))
    }
})


let name, value;
class MoviePage extends Component {

    state = {
        newReview: {Name: "", Review: "", MovieId: this.props.currentMovie._id},
        ReviewerName:"",
        Review: "",
        smShow: false,
        setSmShow: false,
        Show: false,
        setShow: false,
        addReviewResponse:"",
        deleteReviewResponse: ""
    }
    
    fetchMovieID = () => { 
        return window.location.pathname.split("/")[window.location.pathname.split("/").length-1];
    }

    componentDidMount(){
        APICalls.getMoviebyId(this.fetchMovieID()).then(result => {
            this.props.selectMovie(result)
        })

        APICalls.getReviewsByMovieId(this.fetchMovieID()).then(result => {
            this.props.getReview(result)
        })

    }

    handleCloseSm = () => this.setState({smShow: false, setSmShow: false});
    handleShowSm = () => this.setState({smShow: true, setSmShow: true});

    handleClose = () => this.setState({Show: false, setShow: false});
    handleShow = () => this.setState({Show: true, setShow: true});

    handleInputChange = (e) => {
        
        name = e.target.name;
        value = e.target.value;

        this.setState({newReview: {...this.state.newReview, [name]: value}})
    }

    handleAddReview = (e) => {
        e.preventDefault(e)
        APICalls.addReviews(this.state.newReview).then((result) => {
            if(typeof result === "string")
            {
                this.setState({addReviewResponse: result}, () => {
                setTimeout(() => {
                    this.handleClose();
                },3000)
                this.handleShow();
            })}
            else
            {
                this.props.addReview(result)
                this.setState({ReviewerName: "", Review: ""})
            }   
        })}


        handleDeleteReview(e, id) {
            e.preventDefault()
            APICalls.deleteReviews(id).then(result => {
                if(typeof result === "string")
                {
                    this.setState({deleteReviewResponse: result}, () => {
                    setTimeout(() => {
                        this.handleCloseSm();
                    },2000)
                    this.handleShowSm();
                })}
                else
                {
                    this.props.deleteReview(id)
                }
            })
        }


    render() { 
       
        return (
            <div style={{backgroundColor:'black', minHeight:'80vh'}}>
            <div style={{marginLeft:'20vw', display:'flex', flexDirection:'column'}}>
                <div style={{display:'flex', justifyContent:'flex-end', margin:'-3rem 7.5rem 0 0'}}>
                <UpdateMovieModal modalTitle={"Update Movie"} buttonName={"Update Movie Details"} modalDescription={"Only enter fields to update"} movieData={this.props.currentMovie}/>
                <DeleteMovieModal movieId={this.props.currentMovie._id}/>
                <Link to={"/"}>
                <Button variant="primary" type="submit" style={{margin:'5rem 2rem 0 2rem'}}>HomePage</Button>
                </Link>
                </div>
                {
                    this.props.currentMovie.length !== 0 ?
                    <>
                    <section style={{display:'flex', justifyContent:'center', alignItems:'center'}}>
                    <img src={this.props.currentMovie.Poster} alt='Poster' style={{height:'60vh', margin:'3rem 2.5rem 3rem 7.5rem'}} />
                    <div style={{color:'white', alignItems:'center', margin:'3rem 7.5rem 3rem 2.5rem'}}>
                        <h2 style={{paddingBottom:'4rem'}}>{this.props.currentMovie.Name}</h2>
                        <h4>{` ${this.props.currentMovie.Description}`}</h4>
                        <br></br>
                        <h4><b>Rating:</b>{` ${this.props.currentMovie.Rating}`}</h4>
                        <br></br>
                        <h4><b>Genre:</b>{` ${this.props.currentMovie.Genre}`}</h4>
                        <br></br>
                        <h4><b>Cast:</b>{` ${this.props.currentMovie.Cast}`}</h4>
                        <br></br>
                        <h4><b>Runtime:</b>{` ${this.props.currentMovie.Runtime}`}</h4>
                        <br></br>
                        <h4><b>Certificate:</b>{` ${this.props.currentMovie.Certificate}`}</h4>
                    </div>
                    </section>
                    <h3 style={{color:'white', alignSelf:'flex-start', marginLeft:'7.5rem'}}>Reviews</h3>
                    <Form style={{display:'flex', alignItems:'center', columnGap:'1rem', margin:'2rem 7.5rem 2rem 7.5rem'}}>
                        <div style={{width:'100%'}}>
                            <Form.Control type="text" name='Name' value={this.state.ReviewerName} placeholder="Your Name" style={{width:'20rem', marginBottom:'0.5rem'}} onChange={(e) => {this.handleInputChange(e); this.setState({ReviewerName: e.target.value})}}/>
                            <Form.Control as="textarea" name='Review' value={this.state.Review} rows="4" placeholder="Add Review" onChange={(e) => {this.handleInputChange(e); this.setState({Review: e.target.value})}}/>
                        </div>
                        <Button variant="secondary" type="submit" onClick={(e) => {this.handleAddReview(e)}}>Post</Button>
                    </Form>
                    { 
                        [...this.props.reviewByMovie].reverse().map((review) => {
                            return(
                                <section style={{color:'white', display:'flex', justifyContent:'space-between', alignItems:'flex-end', margin:'0rem 7.5rem 3rem 7.5rem', border:'1px solid white', minHeight:'4.5rem'}}>
                                    <div style={{display:'flex'}}>
                                        <p style={{marginLeft:'1rem'}}><b>{review.Name}:</b></p>
                                        <p style={{marginLeft:'1rem'}}>{review.Review}</p>
                                    </div>
                                    <div style={{display:'flex', alignItems:'center', padding:'0 1rem 0 1rem'}}>
                                    <Button variant="danger" type="submit" style={{height:'2.5rem', margin:' 0 0 1rem 0'}} onClick={(e) => {this.handleDeleteReview(e, review._id)}}>Delete</Button>
                                    </div>
                                </section>
                            )
                        })
                    }
                    </>
                : <h1 style={{color:'white', marginTop:'5rem'}}>No Movie to Display</h1>
                }
                <>
                <Modal size="sm" show={this.state.Show} onHide={() => this.setState({Show: false})} aria-labelledby="example-modal-sizes-title-sm">
                    <Modal.Header closeButton>
                    <Modal.Title id="example-modal-sizes-title-sm">Message</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{this.state.addReviewResponse}</Modal.Body>
                    </Modal>
                </>
                <>
                    <Modal size="sm" show={this.state.smShow} onHide={() => this.setState({smShow: false})} aria-labelledby="example-modal-sizes-title-sm">
                    <Modal.Header closeButton>
                    <Modal.Title id="example-modal-sizes-title-sm">Message</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{this.state.deleteReviewResponse}</Modal.Body>
                    </Modal>
                </>
            </div>
            </div>
        );
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(MoviePage);