import React, { Component } from 'react';
import {connect} from 'react-redux'

import FilterDropDowns from './FilterDropdowns';
import AddMovieModal from './AddMovieModal';
import sidebarImg from '../Data/sidebarImg.jpg'
import * as actions from '../redux/actions/moviesAction'

const mapStateToProps = ((state) => {
    return{
        allMovies: state.moviesInfo.moviesData,
    }
})

const mapDispatchToProps = ((dispatch) => {
    return{
        selectMovie: (data) => dispatch(actions.selectedMovie(data)),
        getMovies: (data) => dispatch(actions.getMovies(data)),
    }
})

class SideBar extends Component { 

    component

    render() { 

        return (
            <div style={{position:'fixed', height:"100%", ZIndex:'0', top:'20vh', left:'0', overflowX: 'hidden', width:'20vw', backgroundImage: `url(${sidebarImg})`, display:'flex', flexDirection:'column', alignItems:'center'}}>
                <div style={{display:'flex', flexDirection:'column', alignItems:'centre', marginTop:'5rem'}}>
                <FilterDropDowns allMovies={this.props.allMovies}/>
                <AddMovieModal modalTitle={"Add new movie"} buttonName={"Add new Movie"} modalDescription={"Enter Details..."} movieData={{}}/>
            </div>
            </div>
        );
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(SideBar);