import React, { Component } from 'react';
import {connect} from 'react-redux'
import {Link} from 'react-router-dom';

import * as APICalls from './APICalls';
import Card from 'react-bootstrap/Card';
import * as actions from '../redux/actions/moviesAction'
import Spinner from './Spinner';

const mapStateToProps = ((state) => {
    return{
        allMovies: state.moviesInfo.moviesData,
    }
})

const mapDispatchToProps = ((dispatch) => {
    return{
        getMovies: (data) => dispatch(actions.getMovies(data)),
        selectMovie: (data) => dispatch(actions.selectedMovie(data))
    }
})

class HomePage extends Component {
    state = { 
        pageMountStatus: false,
     } 

    componentDidMount(){
        APICalls.getMovies().then(result => {this.setState({moviesStock: result, pageMountStatus: true}, () => this.props.getMovies(result))});
    }

    handleInputChange =() => {
        let validMovies = this.state.moviesStock.filter((val) => {
            return (this.state.searchBarValue === "" || val.Name.toLowerCase().includes(this.state.searchBarValue.toLowerCase()));
        })
        this.props.getMovies(validMovies);
    }

    handleClickToMoviePage = (movie) => {
        this.props.selectMovie(movie); 
    }

    render() { 

        return ( this.state.pageMountStatus ? 
            <div style={{backgroundColor:'black', minHeight:'80vh'}}>
             
                <div style={{marginLeft:'20vw', display: 'flex', justifyContent:'space-around', flexWrap: 'wrap'}}>
                    { this.props.allMovies.length !== 0 ? 
                        this.props.allMovies.map((movie) => {
                            return(
                                <Link to={`/${movie._id}`} key={movie._id} style={{textDecoration:"none"}}>
                                    <Card style={{width: '18rem', backgroundColor: 'black', color:'white', margin:'3rem 1.5rem 3rem 1.5rem', border:'1px solid white'}} onClick={() => this.handleClickToMoviePage(movie)}>
                                    <Card.Img variant="top" src={movie.Poster} style={{height:'20rem'}}/>
                                    <Card.Body>
                                        <Card.Title style={{textDecoration:'underline', marginBottom:'1rem'}}>{movie.Name}</Card.Title>
                                        <Card.Text>{movie.Description}</Card.Text>
                                        <Card.Text><b>Genre:</b>{` ${movie.Genre}`}</Card.Text>
                                        <Card.Text><b>IMDB Rating:</b>{` ${movie.Rating}`}</Card.Text>
                                    </Card.Body>
                                    </Card>
                                </Link>
                                
                            )
                        })
                        : <h1 style={{color:'white', marginTop:'5rem'}}>No such Movie</h1>
                    }
                </div>
            </div>
            : <div style={{backgroundColor:'black', margin:'0 0 0 8rem', minHeight:'80vh', display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
                <Spinner /> 
                <span style={{color:'white', marginTop:'1.5rem', fontSize:'2rem', fontWeight:'bold'}}>Loading...</span>
            </div>
        );
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(HomePage);